<script type="text/javascript">
var sitePath	= '<?php echo $sitepath; ?>';
var page = '<?php echo $page; ?>';
var ApiEndpoint = sitePath+'api/index.php';

// templates
var tempDollar = '<i class="fa fa-dollar"></i>';
var tempRupee  = '<i class="fa fa-rupee"></i>';
var slug = '<?php echo ((isset($slug))?($slug):('')); ?>';

$(document).ready(function() {
	Checklogin();
	$('#exitSubmit').click(function(){
		var thiz = $(this);
		$('#exitCTAModal help-block').html("");
		var exitEmail = $('#exitEmail').val();
		var exitNumber = $('#exitNumber').val();
		if (!exitEmail && !exitNumber) {
			$('#exitCTAModal .help-block').html("<div class='alert alert-danger'>Please enter either email or phone number</div>");
		} else if (exitEmail && !emailValidate(exitEmail)) {
			$('#exitCTAModal .help-block').html("<div class='alert alert-danger'>Email is wrong!</div>");
		} else {
			thiz.attr('dsiabled', true);
			var req = {};
			req.email = exitEmail;
			req.phone = exitNumber;
			req.action = 'exit-cta';
			$.ajax({
				'type'  : 'post',
				'url'   : ApiEndpoint,
				'data' 	: JSON.stringify(req)
			}).done(function (res) {
				thiz.attr('dsiabled', false);
				$('#exitCTAModal form').get(0).reset();
				res =  $.parseJSON(res);
				if(res.status==1) {
					$('#exitCTAModal .help-block').html("<div class='alert alert-success'>Thank you for the information!</div>");
				} else {
					$('#exitCTAModal .help-block').html("<div class='alert alert-danger'>There was some error, please try again!</div>");
				}
			});
		}
	});
	if (page == 'verify') {
		verifyAccount();
	}
});

/*$(function(){
	var mouseY = 0;
	var topValue = 0;
	window.addEventListener("mouseout",function(e){
		mouseY = e.clientY;
		if(mouseY<topValue) {
			var diffPopTime = $.now()-parseInt(localStorage.getItem('popTime'));
			//console.log(diffPopTime);
			if((localStorage.getItem('popState') != 'shown') || (localStorage.getItem('popState') == 'shown' && diffPopTime>5000)) {
				$("#exitCTAModal").modal('show');
				localStorage.setItem('popState','shown');
				localStorage.setItem('popTime',$.now());
			}
		}
	},
	false);
});*/

function Checklogin(){
	var req = {};
	var res;
	req.action = 'check-login';
	$.ajax({
		'type'  : 'post',
		'url'   : ApiEndpoint,
		'data' 	: JSON.stringify(req)
	}).done(function (res) {
		res =  $.parseJSON(res);
		if(res.status==1){
			loginstatus = 1;
			userId = res.userId;
			userRole = res.userRole;
		}
	});
}

function getUrlParameter(sParam)
{
	sParam = sParam.toLowerCase();
	var sPageURL = window.location.search.substring(1).toLowerCase();
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
}
$(document).on('click','.scrolltodiv', function(event) {
	event.preventDefault();
	var target = "#" + this.getAttribute('data-target');
	$('html, body').animate({
		scrollTop: $(target).offset().top
	}, 1000);
});
var toggleDataMore = function(objLink,objData) {
	var dataMore = objLink.attr("data-more");
	if(dataMore == "full") {
		objData.css("height","auto");
		objLink.attr("data-more","less").html("Hide details");
	} else {
		if(objData.hasClass('js-full-instructor-details')) {
			objData.css("height","100px");
		} else {
			objData.css("height","100px");
		}
		objLink.attr("data-more","full").html("Full details");
	}
}
/**
* Funtion to format single digit number into double digit i.e. append one before item
*
* @author Rupesh Pandey
* @date 23/05/2015
* @param integer input number
* @return integer two digit number
*/
function format(number) {
	if(number < 10)
		return '0' + number;
	else
		return number;
}

function percentage(number, total) {
	if(total == 0)
		return 0;
	return (parseInt(number)/parseInt(total)) * 100;
}

function calcDisplayTimeWords(totalDuration) {
	var hours = parseInt( totalDuration / 3600 ) % 24;
	var minutes = parseInt( totalDuration / 60 ) % 60;
	var seconds = totalDuration % 60;

	if(hours>0) {
		if(minutes>0 && minutes<=15) {
			return hours+".25h";
		} else if(minutes>15 && minutes<=30) {
			return hours+".5h";
		} else if(minutes>30 && minutes<=45) {
			return hours+".75h";
		} else {
			return hours+"h";
		}
	} else if(minutes>0) {
		if(seconds>0 && seconds<=15) {
			return minutes+".25m";
		} else if(seconds>15 && seconds<=30) {
			return minutes+".5m";
		} else if(seconds>30 && seconds<=45) {
			return minutes+".75m";
		} else {
			return minutes+"m";
		}
	} else {
		return seconds+" s";
	}
}

function calcDisplayTime(totalSec) {
	var hours = parseInt( totalSec / 3600 ) % 24;
	var minutes = parseInt( totalSec / 60 ) % 60;
	var seconds = totalSec % 60;

	if(hours>0) {
		return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	} else {
		return (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	}
}

function formatContactNumber(user) {
	var num1= "", num2 = "";
	if(user.contactMobilePrefix != '' && user.contactMobile != '') {
		num1 = user.contactMobilePrefix + '-' + user.contactMobile;
	}
	
	if(user.contactLandlinePrefix != '' && user.contactLandline != '') {
		num2 = user.contactLandlinePrefix + '-' + user.contactLandline;
	}
	
	return (num1 + ', ' + num2).replace(/,\s$/,'').replace(/^,\s/,'');
}

function removeLoader() {
	//$('#loader').remove();
}

function emailValidate(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(regex.test(email))
		return true;
	return false;
}
</script>