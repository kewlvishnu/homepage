<script type="text/javascript">
var roleuser=0;
$(document).ready(function() {
	addResetListener(getUrlParameter('id'));
	
	$("#signIn").click(function(e) {
		e.preventDefault();
		$('.error-msg').addClass("hide");
		var user=$('#user').val();
		var pwd=$('#pwd').val();
		if(user.length<3)
			$('#userError').html("A valid username is required.").removeClass("hide");
		if(pwd.length<6)
			$('#pwdError').html("A valid password is required.").removeClass("hide");
		if(user.length>=3 && pwd.length>=6) {
			req = {
					'action'	: 'login',
					'username'	: user,
					'password'	: pwd,
					'userRole'	: $('#userRole').val(),
					'remember' : (($('#remember').prop("checked"))?1:0)
				};
			req=JSON.stringify(req);
			$.post(sitePath+'api/', req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				//console.log(res);
				if(res.valid == false) {
					if(res.reset == 1) {
						$('#signInError').html('<p class="text-danger">'+res.reason + '<a href="#" class="resend-link">Resend email</a></p>').removeClass("hide");
						addResetListener(res.userId);
					}
					else
						$('#signInError').html('<p class="text-danger">'+res.reason+'</p>').removeClass("hide");
				}
				if(res.valid == true && res.userRole == 4) {
					//console.log(sitePath+"student/");
					window.location = sitePath+"student/";
				}
				else if(res.valid == true && (res.userRole == 1 || res.userRole == 2))
				{
					//console.log(sitePath+"manage/");
					window.location = sitePath+"manage/";
				}
				else if(res.valid == true)
				{
					//console.log(sitePath+"admin/dashboard.php");
					window.location = sitePath+"admin/dashboard.php";
				}
			});
		}
	});
    $('#userRole').on('change',function(){
        $('#signInError').html('').addClass("hide");
    });
   	$('#resetPassword').click(function() {
		$('.error-msg').addClass("hide");
		var fuser=$('#fuser').val();
		if(fuser.length < 6)
			$('#fuserError').html("A valid username is required.").removeClass("hide");
		if(fuser.length >= 6) {
			var req = {};
			req.action = 'send-reset-password-link';
			req.username = fuser;
			req.userRole = $('#fuserRole').val();
			req = JSON.stringify(req);
			$.post(sitePath+'api/', req).success(function(resp) {
				res = jQuery.parseJSON(resp);
				$('#forgetError').html(res.message).removeClass("hide");
			});
		}
	});
	
	function addResetListener(userId) {
		$('.resend-link').off('click');
		//resend verification link
		$('.resend-link').on('click', function() {
			var req = {};
			var res;
			req.id = userId;
			if(req.id == undefined)
				toastr.error("Some error occurred contact admin.");
			req.action = 'resend-verification-link';
			$.ajax({
				'type'	:	'post',
				'url'	:	sitePath+'api/',
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					toastr.error(res.message);
				else
					toastr.error("Verification mail sent. Please check your mailbox.");
			});
		});
	}
	
});
</script>