<script type="text/javascript">
function verifyAccount() {
	var query = slug.split('/');
	if(query[1] != undefined && query[1] != 0) {
		var req = {};
		req.action = 'account-validation';
		req.userId = query[0];
		req.token = query[1];
		if(req.userId != undefined && req.token != undefined) {
			req = JSON.stringify(req);
			$.post(ApiEndpoint, req).done(function(resp) {
				removeLoader();
				addResetListener(query[0]);
				res = jQuery.parseJSON(resp);
				if(res.status == 0)
					$('#verifyError').html(res.message);
				if(res.status == 1) {
					if(res.userRole == 4) {
						$('#verifyError').html("Account has been verified. Please <a href='#loginModal' data-toggle='modal' data-dismiss='modal'>click here to Login</a>");
						$('.js-verify-more p').remove();
						$('.js-verify-more>a:first-child').remove();
					} else {
						$('#verifyError').html("Account has been verified, redirecting you to home page in 5 seconds");
						$('.js-verify-more').remove();
						// Your application has indicated there's an error
						window.setTimeout(function(){
							// Move to a new location or you can do something else
							window.location.href = sitepathMarket+'teach';
						}, 5000);
					}
				}
			});
		}
		else
			$('#verifyError').html("Your Verification link seems to be invalid");
	}
}
function addResetListener(userId) {
	$('.resend-link').off('click');
	//resend verification link
	$('.resend-link').on('click', function() {
		var req = {};
		var res;
		req.id = userId;
		if(req.id == undefined)
			toastr.error("Some error occurred contact admin.");
		req.action = 'resend-verification-link';
		$.ajax({
			'type'	:	'post',
			'url'	:	ApiEndpoint,
			'data'	:	JSON.stringify(req)
		}).done(function(res) {
			res = $.parseJSON(res);
			if(res.status == 0)
				toastr.error(res.message);
			else
				toastr.success("Verification mail sent. Please check your mailbox.");
		});
	});
}
</script>