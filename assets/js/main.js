$(document).ready(function(){
	$('.js-user-type').click(function(){
		var userType = $(this).attr('data-type');
		if(userType == "institute") {
			$('.js-professor').addClass('hide');
			$('.js-institute').removeClass('hide');
		} else if(userType == "professor" || userType == "student") {
			$('.js-institute').addClass('hide');
			$('.js-professor').removeClass('hide');
		}
	});

	var sync1 = $("#sync1");
	var sync2 = $("#sync2");

	sync1.owlCarousel({
		singleItem : true,
		slideSpeed : 1000,
		navigation : true,
		navigationText : ["<i class='fa fa-arrow-left'></i>","<i class='fa fa-arrow-right'></i>"],
		pagination : false,
		afterAction : syncPosition,
		responsiveRefreshRate : 200,
		afterInit : function(el){
			el.find(".owl-item").eq(0).find("img").css("transition", "1s");
			el.find(".owl-item").eq(0).find("img").css("transform", "scale(1)");
		},
		beforeMove : function(el){
			console.log(this);
			var previous = this.prevItem;
			el.find(".owl-item").eq(previous).find("img").css("transition", "1s");
			el.find(".owl-item").eq(previous).find("img").css("transform", "scale(0.5)");
		},
		afterMove : function(el){
			el.find(".owl-item").find("img").css("transform", "scale(0.5)");
			var current = this.currentItem;
			el.find(".owl-item").eq(current).find("img").css("transition", "1s");
			el.find(".owl-item").eq(current).find("img").css("transform", "scale(1)");
		}
	});

	sync2.owlCarousel({
		items : 9,
		itemsDesktop      : [1199,9],
		itemsDesktopSmall     : [979,9],
		itemsTablet       : [768,6],
		itemsMobile       : [640,4],
		itemsMobile       : [481,3],
		
		pagination:false,
		responsiveRefreshRate : 100,
		afterInit : function(el){
			el.find(".owl-item").eq(0).addClass("active");
			el.find(".owl-item").eq(0).addClass("synced");
		}
	});
	
	function syncPosition(el){
		var current = this.currentItem;
		$("#sync2")
			.find(".owl-item")
			.removeClass("active")
			.eq(current)
			.addClass("active");
		$("#sync2").find(".owl-item").removeClass("synced");
		for (var i = 0; i < (this.currentItem+1); i++) {
			$("#sync2").find(".owl-item").eq(i).addClass("synced");
		};
		if($("#sync2").data("owlCarousel") !== undefined){
			center(current)
		}
	}
	
	$("#sync2").on("click", ".owl-item", function(e){
		e.preventDefault();
		var number = $(this).data("owlItem");
		sync1.trigger("owl.goTo",number);
	});
	 
	function center(number){
		var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
		var num = number;
		var found = false;
		for(var i in sync2visible){
			if(num === sync2visible[i]){
				var found = true;
			}
		}

		if(found===false){
			if(num>sync2visible[sync2visible.length-1]){
				sync2.trigger("owl.goTo", num - sync2visible.length+2)
			} else {
				if(num - 1 === -1){
					num = 0;
				}
				sync2.trigger("owl.goTo", num);
			}
		} else if(num === sync2visible[sync2visible.length-1]){
			sync2.trigger("owl.goTo", sync2visible[1])
		} else if(num === sync2visible[0]){
			sync2.trigger("owl.goTo", num-1)
		}

	}
	$(".form-3d .form-control").keyup(function(e){
		if(e.which != 9) {
			var disError = $(this).closest("form").find(".help-block");
			disError.html('').closest('.form-group').removeClass('has-success').removeClass('has-error');
		}
	});
	var formStudent = {
		'type' : "Student",
		'addressCountryId' : 0,
		'agree' : 0,
		'action' : 'register-short',
		'userType' : 'student',
		'userRole' : 4,
		'gender' : 0
	};
	var formProfessor = {
		'type' : "Professor",
		'userInstituteTypes' : [ ],
		'stucturedInsTypes' : { },
		'addressCountryId' : 1,
		'userSchoolBoard' : 0,
		'userSchoolClasses' : 0,
		'agreeOn1' : 0,
		'agreeOn2' : 0,
		'action' : 'register-short',
		'userType' : 'professor',
		'userRole' : 2,
		'gender' : 0
	};
	var formInstitute = {
		'type' : "Institute",
		'userInstituteTypes' : [ ],
		'stucturedInsTypes' : { },
		'addressCountryId' : 1,
		'userSchoolBoard' : 0,
		'userSchoolClasses' : 0,
		'agreeOn1' : 0,
		'agreeOn2' : 0,
		'action' : 'register-short',
		'userType' : 'institute',
		'userRole' : 1
	};
	function emailValidation(element,user) {
		var frmGroup=element.closest('.form-group');
		var disError=element.closest('form').find('.help-block');
		var frmError=disError.closest('.form-group');
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(regex.test(element.val())) {
			frmGroup.removeClass('has-error');
			var req = {};
			req.action = 'validate-email';
			req.email = element.val();
			req.role = user.userRole;
			req=JSON.stringify(req);
			$.post(ApiEndpoint, req).success(function(resp) {
				res=jQuery.parseJSON(resp);
				if(res.valid == false) {
					frmGroup.removeClass('has-success').addClass('has-error');
					disError.html("Email address already registered please select a different one.");
					frmError.removeClass('has-success').addClass('has-error');
					//element.focus();
				}
				else {
					frmGroup.removeClass('has-error').addClass('has-success');
					disError.html("");
					frmError.removeClass('has-success').removeClass('has-error');
					return true;
				}
			});
		}
		else {
			frmGroup.removeClass('has-success').addClass('has-error');
			disError.html("Doesn't seem to be a valid email address.");
			frmError.removeClass('has-success').addClass('has-error');
			//element.focus();
		}
		return false;
	}
	$("#inputEmailAddress").blur(function(){
		if ($('[name=optionUserType]:checked').attr('data-type') == "professor") {
			emailValidation($(this),formProfessor);
		} else if ($('[name=optionUserType]:checked').attr('data-type') == "student") {
			emailValidation($(this),formStudent);
		} else {			
			emailValidation($(this),formInstitute);
		};
	});
	$("#btnTeachSignup").click(function(){
		var disError 				= $("#teachSignup").find('.help-block');
		var frmError 				= disError.closest('.form-group');
		var userType				= $('[name=optionUserType]:checked').attr('data-type');
		if (userType == "professor" || userType == "student") {
			var inputFirstName		= $("#inputFirstName");
			var inputLastName		= $("#inputLastName");
		} else {
			var inputInstituteName	= $("#inputInstituteName");
		}
		var inputEmailAddress		= $("#inputEmailAddress");
		var inputPassword			= $("#inputPassword");
		var selectCountry 			= $("#selectCountry");
		var inputContactMobile		= $("#inputContactMobile");
		if((userType == "professor" || userType == "student") && inputFirstName.val().length<3) {
			disError.html("First Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputFirstName.focus();
		} else if((userType == "professor" || userType == "student") && inputLastName.val().length<3) {
			disError.html("Last Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputLastName.focus();
		} else if(userType == "institute" && inputInstituteName.val().length<3) {
			disError.html("Institute Name is invalid.");
			frmError.removeClass('has-success').addClass('has-error');
			inputInstituteName.focus();
		} else if($(inputEmailAddress).closest('.form-group').hasClass('has-error') ||
					(!$(inputEmailAddress).closest('.form-group').hasClass('has-error') && !$(inputEmailAddress).closest('.form-group').hasClass('has-success'))
				) {
			if (userType == "professor") {
				emailValidation(inputEmailAddress,formProfessor);
			} else if (userType == "student") {
				emailValidation(inputEmailAddress,formStudent);
			} else {
				emailValidation(inputEmailAddress,formInstitute);
			}
		} else if(inputPassword.val().length<6) {
			disError.html("A valid password is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputPassword.focus();
		} else if(!selectCountry.val()) {
			disError.html("Country is required.");
			frmError.removeClass('has-success').addClass('has-error');
			selectCountry.focus();
		} else if(!inputContactMobile.val()) {
			disError.html("Mobile number is required.");
			frmError.removeClass('has-success').addClass('has-error');
			inputContactMobile.focus();
		} else {
			if (userType == "student") {
				formStudent.firstName 	  = inputFirstName.val();
				formStudent.lastName 		  = inputLastName.val();
				formStudent.email 		  = inputEmailAddress.val();
				formStudent.password 		  = inputPassword.val();
				formStudent.addressCountryId= selectCountry.val();
				formStudent.contactMobilePrefix = $('#inputPrefixContactMobile').val();
				if(!$('#inputPrefixContactMobile').val())
					formStudent.contactMobilePrefix="+91";
				formStudent.contactMobile   = inputContactMobile.val();
				formStudent 				  = JSON.stringify(formStudent);
				$.post(ApiEndpoint, formStudent).success(function(resp) {
					res=jQuery.parseJSON(resp);
					if(res.status == 1) {
						$("#registerModal").modal('hide');
						$("#registerSuccessModal").modal('show');
						addResetListener(res.userId);
						$('#teachSignup')[0].reset();
						$('#teachSignup .form-group').removeClass('has-error').removeClass('has-success');
					}
					if(res.status == 0)
						alert(res.message);
				});
			} else if (userType == "professor") {
				formProfessor.firstName 	  = inputFirstName.val();
				formProfessor.lastName 		  = inputLastName.val();
				formProfessor.email 		  = inputEmailAddress.val();
				formProfessor.password 		  = inputPassword.val();
				formProfessor.addressCountryId= selectCountry.val();
				formProfessor.contactMobilePrefix = $('#inputPrefixContactMobile').val();
				if(!$('#inputPrefixContactMobile').val())
					formProfessor.contactMobilePrefix="+91";
				formProfessor.contactMobile   = inputContactMobile.val();
				formProfessor 				  = JSON.stringify(formProfessor);
				$.post(ApiEndpoint, formProfessor).success(function(resp) {
					res=jQuery.parseJSON(resp);
					if(res.status == 1) {
						$("#registerModal").modal('hide');
						$("#registerSuccessModal").modal('show');
						addResetListener(res.userId);
						$('#teachSignup')[0].reset();
						$('#teachSignup .form-group').removeClass('has-error').removeClass('has-success');
					}
					if(res.status == 0)
						alert(res.message);
				});
			} else {
				formInstitute.instituteName	  = inputInstituteName.val();
				formInstitute.email 		  = inputEmailAddress.val();
				formInstitute.password 		  = inputPassword.val();
				formInstitute.addressCountryId= selectCountry.val();
				formInstitute.contactMobilePrefix = $('#inputPrefixContactMobile').val();
				if(!$('#inputPrefixContactMobile').val())
					formInstitute.contactMobilePrefix="+91";
				formInstitute.contactMobile   = inputContactMobile.val();
				formInstitute 				  = JSON.stringify(formInstitute);
				$.post(ApiEndpoint, formInstitute).success(function(resp) {
					res=jQuery.parseJSON(resp);
					if(res.status == 1) {
						$("#registerModal").modal('hide');
						$("#registerSuccessModal").modal('show');
						addResetListener(res.userId);
						$('#teachSignup')[0].reset();
						$('#teachSignup .form-group').removeClass('has-error').removeClass('has-success');
					}
					if(res.status == 0)
						alert(res.message);
				});
			}
		}
		return false;
	});

	function addResetListener(userId) {
		$('.resend-link').off('click');
		//resend verification link
		$('.resend-link').on('click', function() {
			var req = {};
			var res;
			req.id = userId;
			if(req.id == undefined)
				alert("Some error occurred contact admin.");
			req.action = 'resend-verification-link';
			$.ajax({
				'type'	:	'post',
				'url'	:	ApiEndpoint,
				'data'	:	JSON.stringify(req)
			}).done(function(res) {
				res = $.parseJSON(res);
				if(res.status == 0)
					alert(res.message);
				else
					alert("Verification mail sent. Please check your mailbox.");
			});
		});
	}
});